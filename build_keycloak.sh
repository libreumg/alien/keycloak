#!/bin/bash

chmod 755 keycloak/DEBIAN
chmod 644 keycloak/DEBIAN/*
chmod 755 keycloak/DEBIAN/postinst
chmod 755 keycloak/DEBIAN/prerm

bash build_deb.sh keycloak
