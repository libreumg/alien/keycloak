function load_nexus_creds {
	if [ -r ~/.nexus_token ] ; then
		if [ -z "$user" -o -z "$secret" -o -z "$nexus" ] ; then
			. ~/.nexus_token
		fi
		if [ -z "$user" -o -z "$secret" -o -z "$nexus" ] ; then
			>&2 echo "Need access to a nexus server, run in CI/CD with variables set or create ~/.nexus_token";
			exit 1;
		fi
	fi
}

function set_github_package_version {
  l=$(curl https://github.com/$2/$2/releases 2>/dev/null | grep "$2/$2/releases/tag.[0-9].*primary" | head -n 1)
  f=$(echo "$l" | awk -F/ '{print $6}' | sed -e "s/\".*$//")
  eval "$1='$f'"
}

function set_amd64_debian_package {
  eval "$1='$2'_'$3'_amd64.deb"
}

function is_package_available {
  return $(curl -s -X 'GET' "$1/service/rest/v1/search/assets?name=$3&version=$4&repository=$5" | grep -F $2 > /dev/null)
}

function download_and_unpack {
  d=$(basename $1)
  if [ -f $d ]
  then 
    echo "file $d already downloaded"
  elif [ "keycloak-nightly.tar.gz" == "$d" ]
  then
    echo "found the nightly build version, aborting..."
    return 1 # 1 is false
  else
    wget $1
  fi
  f=$(pwd)
  mkdir -p $2
  cp keycloak*.tar.gz $2
  cd $2
  tar -zxf *.tar.gz
  rm -f *.tar.gz
  cd $f
  return 0
}

function repair_symlink_and_version {
  f=$(pwd)
  cd "$3/DEBIAN"
  sed -i control -e "s/Version: .*/Version: $1/"
  echo "correcting version to $2 in postinst..."
  # use | instead of / is allowed for sed if the replacement contains /
  sed -i postinst -e "s/VERSION/$2/g"
  cd "$f"
}

load_nexus_creds

apt-get update -y ; apt-get install -y wget curl

n="keycloak"
set_github_package_version z keycloak
v="$z-0"
set_amd64_debian_package p $n $v
r="ship-debian-bookworm"
u="https://github.com/keycloak/keycloak/releases/download/$z/keycloak-$z.tar.gz"

if (is_package_available $nexus $p $n $v $r)
then
  echo $p "already exists in $r on $nexus -- won't upload again, force not yet implemented"
  exit 0
else
  echo "$p not found in $r on $nexus, so building it..."
  if (download_and_unpack $u "keycloak/opt")
  then 
    repair_symlink_and_version $v $z "keycloak"
    ./build_keycloak.sh
  fi
fi
