#!/bin/bash

### for development environments, use this command:
# /opt/keycloak/bin/kc.sh start-dev

### if wanted, build keycloak configuration on will
## for using the old pre-quarkus path, append auth again
# /opt/keycloak/bin/kc.sh build --http-relative-path=/auth

# for productive environments, use
/opt/keycloak/bin/kc.sh start
