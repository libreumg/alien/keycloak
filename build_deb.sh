#!/bin/bash

if [ -z "$1" ]
then
  echo ""
  echo "usage: $0 folder"
  echo ""
else
  N="$1"
  V=`more $N/DEBIAN/control | grep "Version:" | sed "s/Version: //"`
  dpkg -b $N "$N"_$V.deb
fi
